
if (mq.matches) {
	var acc = document.getElementsByClassName("accordian");
	var i;
	for (i = 0; i < acc.length; i++) {
	    acc[i].addEventListener("click", function() {
	        this.classList.toggle("active");
	        var panel = this.nextElementSibling;
	        if (panel.style.display === "block") {
	            panel.style.display = "none";
	        } else {
	            panel.style.display = "block";
	        }
	    });
	}
}

// function myFunction(x) {
// 	var menu = document.getElementsByClassName("responsive-menu")[0];
//     x.classList.toggle("change");
//     console.log(menu);
//     menu.style.display="block";
// }
