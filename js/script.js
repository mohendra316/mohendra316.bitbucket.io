const mq = window.matchMedia( "(max-width: 768px)" );
var list = document.getElementById('slider').children;
var currentItem = list[0];
var listLength = list.length;
var current = 0;
function navigate(direction){
	currentItem.classList.remove("active");
	current += direction;
	if(direction == -1 && current<0){
		console.log(current);
		current = listLength-1;
	}
	if(direction ==1 && current>listLength-1){
		current = 0;
	}
	currentItem = list[current];
	currentItem.classList.add("active");
}


document.getElementById('right-control').addEventListener('click', function () {
    	navigate(1);
    });
document.getElementById('left-control').addEventListener('click', function () {
    	navigate(-1);
    });

